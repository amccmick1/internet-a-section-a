# Internet A - Section A

[Live web application demo](http://54.68.21.95/AIDA/SectionA/c3341430/) built on responsive framework  [Skel](http://getskel.com/),  showcasing a part of my developing skill at building the various components from which complex web applications are formed. This demo has been produced as supporting evidence for a third-year assignment on a BSC(Hons) Computing degree and has been built with:

* HTML5
* CSS3
* OO PHP
* jQuery

## Hosting

The live demo of the site is hosted by Amazon Web Services (AWS) using EC2 Elastic Computiing. The advantages of utilising this technology is that you have large creative freedom to customise the infrastructure your web application resides in. 

## Setup
*A brief summary of the process of creating and hosting the site. This web application was setup to be hosted on an EC2 Ubuntu Server from a PC running an Ubuntu Desktop distro and as such any example commands listed will be specific to this creative process*

In order to host your site through AWS you need to create an account and login to the **AWS console**. You will then need to create an **instance** in EC2. The instance in this case will be your web server and will automatically create with it storage for all your files. During the creation process you will create a **security group** which will control permissions for inbound and outbound connections and their protocol. By default it allows inbound SSH connections on port 22 for remote bash control of the server. To use the instance as a web server you will need to add an inbound HTTP traffic rule on port 80. The last step of the instance creation process will prompt you to create a pair of **ssh keys** with the file ext .pem; one private to be stored on the server and one public for you to download and use to access the server. 

Once you have downloaded your key you'll need to change the file permissions. Navigate to the directory you saved your .pem key and issue the following command:


```sudo chmod 600 <your-ssh-key-name.pem> ```

Next, create an **Elastic IP** in your AWS console and **assign** it to your instance. This will allow you to keep a static IP address for connecting to your instance.

You can then use your Elastic IP to connect ssh to the server. First open a new terminal then navigate to the directory you saved your public key and type:

```ssh -i <your-ssh-key-name.pem> ubuntu@<ip-address>```

This will give you access to the terminal on the server. The next step is to customise your instance for use as a web server.

## Install

*This section assumes you have an ssh connection to your server running in the terminal and have root privileges*

For permissions to customise your instance for use as a web server you can switch to the root user in the terminal

```sudo bash```

Proceed to install the Apache web server

```apt-get install apache2```

Install PHP

```apt-get install php5 libapache2-mod-php5 php5-mcrypt```

Install Curl

```apt-get install php5-curl```

Install XSL

```apt-get install xsl```

By default Apache will store all web content in the ```/var/www/html``` directory

## Database


*This section assumes you have an ssh connection to your server running in the terminal and have root privileges*

To use the MySQL cli install with

```apt-get install mysql-server libapache2-mod-auth-mysql php5-mysql```    

```mysql_install_db```    

```/usr/bin/mysql_secure_installation```

Then login into MySQL with the credentials you filled out during the installation

```mysql -u <user> -p```

This will prompt for your password. Once entered correctly you will be greeted with the MySQL cli

```mysql> ```

You can then create a database with standard MySQL syntax

```mysql> CREATE DATABASE 'database_name';```

To switch to the created database 

```mysql> USE 'database_name';```

Create tables and manipulate with INSERT  queries. In the demo I used

```mysql> CREATE TABLE tunes(id INT(5) key auto_increment, artist VARCHAR(100), song VARCHAR(150));```

```mysql> INSERT INTO tunes VALUES('', 'Bullet for My Valentine', "Tears Don't Fall");```

Once finished just exit MySQL as you would any other terminal process (```CTRL``` + ```C```)


#####NOTE:
Should you need to change your password at any point you can issue the command*

```SET PASSWORD FOR <user>@localhost = PASSWORD('yourpassword');```

## Mail
So that you can use the php ```mail($to, $subject, $message, $header)```function you will need to make some changes to your server's configuration. First you will need to install MSMTP with the component which will allow ssl certificate authentication

```apt-get install msmtp ca-certificates```

Create and edit the configuration file

```nano /etc/msmtprc```

Paste this code into your ```/etc/msmtprc``` file

```defaults```  
```tls on```  
```tls_starttls on```  
```tls_trust_file /etc/ssl/certs/ca-certificates.crt```  
```account default```  
```host smtp.gmail.com``` 
```port 587```  
```auth on```  
```user <username@gmail.com>```  
```password <mypass>```  
```from <username@gmail.com>```  
```logfile /var/log/msmtp.log```  

Ensure the file permissions are correct

```chmod 0644 /etc/msmtprc```

Test with

```echo -e "Subject: Test Mail\r\n\r\nThis is a test mail" |msmtp --debug --from=default -t username@gmail.com```

If you cannot read the message at this stage then Google's security policy has kicked in meaning you need to authenticate access to your emails from a new machine. You can do this by installing and running the w3m terminal browser

```apt-get install w3m w3m-img```

And signing into your google account here

```w3m https://mail.google.com```

Once logged in quit w3m by pressing ```q``` and saying yes (```y```) to the quit prompt

Try the test again

```echo -e "Subject: Test Mail\r\n\r\nThis is a test mail" |msmtp --debug --from=default -t <username@gmail.com>```

If you are able to read the message then the test has been successful and you can continue to map MSMTP message handling to the PHP mail function. Next open your php.ini file for editing with

```nano /etc/php5/apache2/php.ini```

Search for 'sendmail_path', uncomment by remove preceeding ' ; ' and change to 

```sendmail_path = '/usr/bin/msmtp -t'```

Restart Apache

```service apache2 restart```

Now calling the ```mail($to, $subject, $message, $header)``` function in PHP will use msmtp for mail handling