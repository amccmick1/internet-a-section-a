<?php

// Auto Load Required Classes
function __autoload($class_name) {
    if(file_exists('includes/classes/'. $class_name . '.php')) {
        require_once('includes/classes/'.$class_name . '.php');    
    } else {
        throw new Exception("Unable to load $class_name.");
    }
}

echo "<hr /><br /><h3>Load Class</h3>";
echo "<pre>
function __autoload(\$class_name) {
    if(file_exists('includes/classes/'. \$class_name . '.php')) {
        require_once('includes/classes/'.\$class_name . '.php');    
    } else {
        throw new Exception(\"Unable to load \$class_name.\");
    }
}</pre>";

// Create a new songs
echo "<h3>Create new Object</h3><p>\$tune1 = new OOTest(\"Clean Bandit\", \"Rather Be\");</p><br />";
$tune1 = new OOTest("Clean Bandit", "Rather Be");
 
// Get song artist
echo "<h3>Get OOTest Artist</h3>";
echo "<p>\$tune1->getArtist() = ".$tune1->getArtist()."</p><br />";
 
// Change a Song's Artist
echo "<h3>Set OOTest Song</h3>";
echo "<p>\$tune1->changeSong(\"A & E\");</p><br />";

$tune1->changeSong("A & E");

// Get song name
echo "<h3>Get OOTest Song</h3>";
echo "<p>\$tune1->getSong() = ".$tune1->getSong()."</p><br />";
 
// Output OOTest Object
echo "<h3>Ouput OOTest Object Array</h3><br />";
echo "<p>Tune 1: ", print_r($tune1, TRUE), "</p><br />";

//create a new object collection (of songs)
echo "<h2>Object Collection</h2><hr /><br />";
echo "<h3>Create new ObjectCollection</h3>";
echo "<p>\$ObjColl = new ObjectCollection();</p><br />";
$ObjColl = new ObjectCollection();

//create a second song to use for Object Collection
echo "<h3>Create a second Object</h3>";
echo '<p>$tune2 = new OOTest("Fatboy Slim", "Rockafella Skank");</p><br />';
$tune2 = new OOTest("Fatboy Slim", "Rockafella Skank");

//add songs to object collection
echo "<h3>Add Items to Object Collection</h3><br />";
echo "<pre>\$ObjColl->addItem(\$tune1);
\$ObjColl->addItem(\$tune2);</pre><br />";
$ObjColl->addItem($tune1);
$ObjColl->addItem($tune2);

//print information on each item
echo "<h3>Ouput Object Collection Object Array</h3><br />";
for($i = 0; $i < $ObjColl->getItemCount(); $i++){
    echo '<pre>';
    print_r($ObjColl->getItem($i));
    echo '</pre><br />';
}
?>
