<!DOCTYPE HTML>
<HTML>
	<HEAD>
		<?php require 'head.php'; ?>
	</HEAD>
	<BODY>
		<!-- Init -->
		<?php require_once 'config/init.php';?>
	
		<!-- Nav -->
		<?php require 'nav.php'; ?>
	
		<!-- Header -->
		<?php require 'header.php'; ?>
		
		<!-- One -->
		<?php require 'about.php'; ?>
		
		<!-- Two -->
		<section id="two" class="main style2 left dark fullscreen">
			<div class="content box style2">
				<header>
					<h2>Mysqli</h2>
				</header>
				<p>OO PHP Mysqli data fetch from database</p>
					Search<br />
					<form method="GET">
						<div class="row half">
							<div class="6u">					
								<input type="text" id="search" name="search" /><br />
							</div>
							<div class="6u">
								<select name="column" id="column">
									<option value="id">ID</option>
									<option value="artist">Artist</option>
									<option value="song">Song</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="12u">
                                <style scoped>p,a { color:black; } </style>
								<ul class="actions">
									<li><input type="submit" value="Submit" /></li>
									<li><p><a href="rss/create_feed.php">RSS Feed</a></p></li>
                                    <li><p><a href="xml/export_xml.php">Export XML</a></p></li>
                                    <li><p><a href="json/create_json.php">Export JSON</a></p></li>
								</ul>
							</div>
						</div>
					</form>					
				<br />
				<br />
				<hr />
				
				<!-- Display Table populated from Databse with Optional $_GET variables filtering WHERE clauses -->
				<?php require 'includes/functions/display_table.php'; ?>
				
			</div>
			<a href="#three" class="button style2 down anchored">Next</a>
		</section>
		
		<!-- Three -->
		<section id="three" class="main style2 right dark fullscreen">
			<div class="content box style2">
				<header>
					<h2>OOTest</h2>
					<?php include 'ootest_test.php'; ?>
					<br />
				</header>
			</div>
		</section>

		<!-- Media -->
		<section id="media" class="main style3 primary">
			<div class="content container">
				<header>
					<h2>Poptrox</h2>
					<p>Test out this Poptrox Lightbox Gallery by clicking on any of the images below.</p>
				</header>
				
				<!-- Lightbox Gallery  -->
				<div class="container small gallery">
					<div class="row flush images">
						<div class="6u">
                            <!--<a href="assets/img/fulls/01.jpg" class="image fit from-left"><img src="assets/img/thumbs/01.jpg" title="The Anonymous Red" alt="" /></a>-->
                            <a href="http://youtu.be/ivHrgwwVFBE" class="image fit from-left" data-poptrox="youtube,650x650"><img src="assets/img/thumbs/byebyecopyright.png" alt="" title="ByeByeCopyright Music Video" /></a>
                            
                        </div>
						<div class="6u"><a href="assets/img/fulls/02.jpg" class="image fit from-right"><img src="assets/img/thumbs/02.jpg" title="Airchitecture II" alt="" /></a></div>
					</div>
					<div class="row flush images">
						<div class="6u"><a href="assets/img/fulls/03.jpg" class="image fit from-left"><img src="assets/img/thumbs/03.jpg" title="Air Lounge" alt="" /></a></div>
						<div class="6u"><a href="assets/img/fulls/04.jpg" class="image fit from-right"><img src="assets/img/thumbs/04.jpg" title="Carry on" alt="" /></a></div>
					</div>
					<div class="row flush images">
						<div class="6u"><a href="assets/img/fulls/05.jpg" class="image fit from-left"><img src="assets/img/thumbs/05.jpg" title="The sparkling shell" alt="" /></a></div>
						<div class="6u"><a href="assets/img/fulls/06.jpg" class="image fit from-right"><img src="assets/img/thumbs/06.jpg" title="Bent IX" alt="" /></a></div>
					</div>
				</div>
			</div>
		</section>

		<!-- Location -->
		<section id="location" class="main style1 fullscreen"></section>

       	<!-- Api -->
		<section id="api" class="main style3 primary">
			<div class="content container">
				<header>
					<h2>API</h2>
					<?php include 'includes/functions/display_heroes.php'; ?>
				</header>
            </div>
        </section>
			
		<!-- Register -->
		<section id="register" class="main style3 primary">
			<div class="content container">
				
				<!-- If not logged in display Register Section -->
				<?php if(!isset($_SESSION['user'])) { ?>
				<header>
					<h2>Register</h2>
					<p>Enter your details here to register to the site. Registration is free. Once registered, you will be able to login to the site
					whenever you want. Be sure to remember your email address and password for logon access to the site.</p>
					<br />
					<form method="post" action="includes/functions/register.php">
						<div class="row half">
							<div class="6u"><input type="email" name="email" placeholder="Email" pattern="([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})" required /></div>
							<div class="6u"><input title="at least eight characters containing at least one number, one lower, and one upper letter" type="password" name="password" placeholder="Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  /></div>
                            <!-- Add Security Questions, Captchas -->
						</div>
						<div class="row">
							<div class="12u">
								<ul class="actions">
									<li><input type="submit" value="Submit" /></li>
								</ul>
						</div>
					</div>
					</form>
				</header>
				
				<!-- Else if logged in display Account Section -->
				<?php } else { ?>
				<header>
					<h2>Account</h2>
					<p>Welcome to Internet A Section A: <?php echo $_SESSION['user']; ?></p>
					<br />
					<form method="post" action="includes/functions/logout.php">
						<div class="row">
							<div class="12u">
								<ul class="actions">
									<li><input type="submit" value="Logout" /></li>
								</ul>
							</div>
						</div>
					</form>
				</header>
				<?php } ?>
				
			</div>
	</section>
			
	<!-- Contact -->
	<?php require 'contact.php'; ?>

	<!-- Footer -->
	<?php require 'footer.php'; ?>
	
	</BODY>
</HTML>
