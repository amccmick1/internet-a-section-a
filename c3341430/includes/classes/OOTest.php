<?php 

class OOTest
{

  private $_artist;
  private $_song;
 
  public function __construct($artist, $song)
  {
      $this->_artist = $artist;
      $this->_song = $song;
  }
 
  public function changeArtist($newartist)
  {
      $this->_artist = $newartist;
  }
  
  public function changeSong ($newsong) {
  
	$this->_song = $newsong;
  
  }
  
  public function getArtist () {
  
	return $this->_artist;
  }
  
    public function getSong () {
  
	return $this->_song;
  }
}

?>