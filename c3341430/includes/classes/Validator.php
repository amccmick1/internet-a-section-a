<?php

 class Validator 
 {

	public $data;
	
	public function __construct($data = NULL)
    {
		$this->data = $data;
	}
	
	public function validate() 
	{
		$this->data = trim($this->data);
		$this->data = stripslashes($this->data);
		$this->data = htmlspecialchars($this->data);
		return $this->data;
	}
 }

?>