<?php 
 class ObjectCollection
{  
	//This is an array to hold line items
	private $items_array;
	
	private $itemCounter; //Count the number of items
 
	public function __construct() {
	    //Create an array object to hold line items
	    $this->items_array = array();
		$this->itemCounter=0; 
	 }
	
	public function getItemCount(){
		return $this->itemCounter;
	}  

	// This will add a new line object to line items array
	public function addItem($item) {
	   $this->itemCounter++;
	   $this->items_array[] = $item;
    }
	
     // This will get an item
    public function getItem($id) {
        return $this->items_array[$id];
    }
}
?>