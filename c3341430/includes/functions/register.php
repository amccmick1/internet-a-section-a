<?php

// Continue Session from Index Page
require_once '../../config/init.php';

// Get Database Credentials
require_once '../../config/config.php';

// Include Database Class
require_once '../classes/MysqliDb.php';

// Include Validation Class
require_once '../classes/Validator.php';

// Create Object of Validator and Validate Post Email
$validate_email = new Validator($_POST['email']);

// Create Object of Database Class and Connect (Called in Constructor)
$db = new MysqliDb ($dbc['hostname'], $dbc['username'], $dbc['password'], $dbc['database']);

$data = Array ('id' => '',
				'email' => $validate_email->data,
               'password' => $db->func('SHA1(?)',Array ($_POST["password"]))
);

$id = $db->insert('users', $data);

if($id)
    $_SESSION['user'] = $validate_email->data;
	
Header('Location: ../../index.php');

?>