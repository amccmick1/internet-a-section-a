<?php
require_once ('api/dota2/config.php');

echo '<br/><h3>XML Heroes Data fetched from Dota 2 API Request, put into a mult-dimensional php array and accessed with a loop</h3>';

// Initialise Mapper for Hero data
$heroes_mapper = new heroes_mapper();

// Load heroes through XML API Request
$heroes = $heroes_mapper->load();

// Return Hero Data From Response (XML Converted to PHP Array) and Output
echo "<p>";
for ($row = 1; $row < sizeof($heroes) - 1; $row++) {  echo $heroes[$row]['localized_name'].", ";   }
echo $heroes[sizeof($heroes)-1]['localized_name'];
echo "</p><br/>";

echo '<h3>Using Local JSON Data to retrieve Image URL source and Hero Names</h3><br/>';

// Fetch Hero Info and Image URL Using Local JSON File Data
$heroes = new heroes();
$heroes->parse(); // decodes JSON file
$id = 95; // Selects hero "Troll Warlord"

// Output Hero and Image fetched from URL
echo '<img src="'.$heroes->get_img_url_by_id($id, false).'" alt="" title="'.$heroes->get_field_by_id($id, "localized_name").'" />
      <h3>'.$heroes->get_field_by_id($id, "localized_name").'</h3>';
?>

