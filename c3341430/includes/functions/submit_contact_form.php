<?php

// Include Math Secure Class
require_once "includes/classes/BasicMathSecurity.php";

// construct the object with the same parameter
$math = new BasicMathSecurity( 'math' );
if( $math->isCorrect() ) {

	require_once 'includes/classes/Validator.php';
	$name = new Validator($_POST['name']);
	$email = new Validator($_POST['email']);
	$message = new Validator($_POST['message']);

	$msg = "Name: ".$name->validate().", Email: ".$email->validate().", Message: ".$message->validate();

	$headers = "";
	
	// Email
	mail($email->validate(), 'AIDA Form Test', $msg, $headers);

	// Confirmation
	echo '<div id="success" class="row"><div class="12u"><style scoped> p { color: #339933; } </style><p>Message has been sent successfully.</p></div></div>';
	
}

?>