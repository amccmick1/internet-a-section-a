<?php
// Get Database Credentials
require_once 'config/config.php';

// Include Database Class
require_once ('includes/classes/MysqliDb.php');

// Creaate new Object of Database Class
$db = new MysqliDb ($dbc['hostname'], $dbc['username'], $dbc['password'], $dbc['database']);

// Validate User Entered Data
require 'includes/classes/Validator.php';

if (isset($_GET['column']))
$col = new Validator($_GET['column']);

if (isset($_GET['search']))
$query = new Validator($_GET['search']);

// Mysqli Select Query
// Filter any GET params through WHERE Clause Query
if(!empty($_GET['search'])) 
$db->where ($col->validate(), $query->validate());

$tunes = $db->get ('tunes');

//Start Table, Start Head		
$html_table = '<table>' . "\n"  . '<thead>';

//New Row	
$html_table .= '<tr>' . "\n";

//Header
$html_table .= '<th>ID</th>';
$html_table .= '<th>Artist</th>';
$html_table .= '<th>Song</th>';	

//End Row, End Head	
$html_table .= '</tr>' . "\n" . '</thead>';

//Start Body		
$html_table .= '<tbody>';

// Fetch Data
if ($db->count > 0) {
    foreach ($tunes as $tune) {
		$html_table .= '<tr>';
		$html_table .= '<td class="id">'.$tune['id'].'</td>';
		$html_table .= '<td class="artist">'.$tune['artist'].'</td>';
		$html_table .= '<td class="song">'.$tune['song'].'</td>';
		$html_table .= '</tr>';	
    }
}

//End Body, End Table
$html_table .= '</tbody>' . '</table>';
	
//Display Table
echo $html_table;

?>