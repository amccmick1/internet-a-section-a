<?php

require_once '../../config/init.php';

session_destroy();

Header('Location: ../../index.php');

?>