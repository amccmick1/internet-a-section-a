<?php
// Get Database Credentials
require_once '../config/config.php';

// Include Database Class
require_once ('../includes/classes/MysqliDb.php');

// Creaate new Object of Database Class
$db = new MysqliDb ($dbc['hostname'], $dbc['username'], $dbc['password'], $dbc['database']);

// Fetch Data
$tunes = $db->get ('tunes');

// Export to JSON
file_put_contents('data.json', json_encode($tunes));

// Redirect to display JSON
header ('Location: display_json.php');
?>
