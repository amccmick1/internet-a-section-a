<!DOCTYPE HTML>
<html>
<head>
    <?php include '../head.php'; ?>
</head>
<body>
    <script>
	$(document).ready(function() {
		$.getJSON( "data.json", function( data ) {
			var items = [];
			$.each( data, function( key, val ) {
				items.push( "<li id='" + key + "'>" + val.artist + " - " + val.song + "</li>" );
			});
			$( "<ul/>", {
				"class": "playlist",
				html: items.join( "" )
			}).appendTo( "body" );
		});
	});
    </script>
</body>
</html>
