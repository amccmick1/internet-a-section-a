<!-- Ribbon -->
<style>
@import url(http://fonts.googleapis.com/css?family=Kite+One);
@import url(http://fonts.googleapis.com/css?family=Bitter:400,700);

*, *:before, *:after{
  margin: 0;
  padding: 0;
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;         /* Opera/IE 8+ */
  -webkit-transition: .15s ease-in;
  -moz-transition: .15s ease-in;
}
html, body{
  min-height: 100%; 
}

.ribbon h1{
  color: #464646;
  font-family: "Roboto Condensed",sans-serif;
  font-weight: 100;
  font-size: 2.5em;
  margin-bottom: .5em;
  text-align: center;
  text-shadow: 0.0625em 0.0625em 0.0625em #666;
}
.ribbon p{
  text-align: center;
  color: #464646;
  margin-bottom: 2em
}
.ribbon p a{
  color: #464646;
}

/*ribbon css */

.ribbon{
  margin: 3em;
  /* IE10 Consumer Preview */ 
  background-image: -ms-radial-gradient(center top, circle farthest-side, #2D3E48 0%, #202E37 100%);
  /* Mozilla Firefox */ 
  background-image: -moz-radial-gradient(center top, circle farthest-side, #2D3E48 0%, #202E37 100%);
  /* Opera */ 
  background-image: -o-radial-gradient(center top, circle farthest-side, #2D3E48 0%, #202E37 100%);
  /* Webkit (Safari/Chrome 10) */ 
  background-image: -webkit-gradient(radial, center top, 0, center top, 487, color-stop(0, #2D3E48), color-stop(1, #202E37));
  /* Webkit (Chrome 11+) */ 
  background-image: -webkit-radial-gradient(center top, circle farthest-side, #2D3E48 0%, #202E37 100%);
  /* W3C Markup, IE10 Release Preview */ 
  background-image: radial-gradient(circle farthest-side at center top, #2D3E48 0%, #202E37 100%);
  width: 5em;
  height: 10em;
  position: fixed;
  top: 0px;
  left: 10px;
  border-top-right-radius: .2em;
  border-top-left-radius: .2em;
  margin: auto;
  font-family: 'Kite One', sans-serif;
  z-index:100000;
}

.ribbon i{
  width: 90%;
  height: 120%;
  display: block;
  margin: auto;
  position: relative;
  border-right: dashed 0.0625em #333;
  border-left: dashed 0.0625em #333;
  overflow: hidden;
}
i span{
  font-family: "Roboto Condensed",sans-serif;
  color: #464646;
  display: block;
  text-align: center;
  top: 50%;
  margin-top: -2em;
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  position: relative;
  text-shadow: 0.0625em 0.0625em 0.0625em #333;
  font-style: normal;
  font-weight: bold;
}
s{
  width: 0.625em;
  height: 0.625em;
  background: #333;
  top: 50%;
  margin-top: -0.3125em;
  -webkit-transform:rotate(45deg);
  -moz-transform:rotate(45deg);
  display: block;
  position: absolute;
  opacity: .5;
}

s:nth-of-type(1){
  margin-left: -1.25em;
  left: 0;
}

.ribbon:before{
  content: "";
  position: absolute;
  bottom: -2.4em;
  left: 0;
  width: 0;
    height: 0;
    border-top: 2.5em solid #202E37;
    border-right: 2.5em solid transparent;
}
.ribbon:after{
  content: "";
  position: absolute;
  bottom: -2.4em;
  right: 0;
  width: 0;
    height: 0;
    border-top: 2.5em solid #202E37;
    border-left: 2.5em solid transparent;
}
</style>
<a href="http://www.offcourseprog.com">
<div class="ribbon">
  <i><span><s></s>Off-Course</span></i>
</div>
</a>
<!- End Ribbon -->
<header id="header">
	
	<!-- Logo -->
	<h1 id="logo">Internet A</h1>
				
	<!-- Nav -->
	<nav id="nav">
		<ul>
			<li><a href="#intro">Intro</a></li>
			<li><a href="#one">About</a></li>
			<li><a href="#two">Mysqli</a></li>
			<li><a href="#three">OOTest</a></li>
			<li><a href="#media">Poptrox</a></li>
			<li><a href="#location">Location</a></li>
            <li><a href="#api">API</a></li>
			<li><a href="#register"><?php if(!isset($_SESSION['user'])) echo 'Register'; else echo 'Account'; ?></a></li>
			<li><a href="#contact">Contact</a></li>
		</ul>
	</nav>
	
</header>
