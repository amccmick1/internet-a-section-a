<?php
// Get Database Credentials
require_once '../config/config.php';

// Include Database Class
require_once ('../includes/classes/MysqliDb.php');

// Creaate new Object of Database Class
$db = new MysqliDb ($dbc['hostname'], $dbc['username'], $dbc['password'], $dbc['database']);

// XML Parsing Function
function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

// Fetch Data
$tunes = $db->get ('tunes');

// Set XML Header
header("Content-Type: text/xml");

// Start XML file, echo parent node
echo '<playlist>';

// Iterate through the rows, printing XML nodes for each
if ($db->count > 0) {
    foreach ($tunes as $tune) {
         // ADD TO XML DOCUMENT NODE
          echo '<song ';
          echo 'name="' . parseToXML($tune['song']) . '" ';
          echo 'artist="' . parseToXML($tune['artist']) . '" ';
          echo '/>';
	}
}

echo '</playlist>'
?>