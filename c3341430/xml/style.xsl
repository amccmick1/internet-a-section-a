<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/rss/channel">
      <html>
          <body>
              <h2>Top 10</h2>
                <table border="1">
                  <tr bgcolor="#9acd32">
                    <th style="text-align:left">Title</th>
                    <th style="text-align:left">Artist</th>
                  </tr>
                  <xsl:for-each select="item">
                      <tr>
                          <td><xsl:value-of select="title"/></td>
                            <td><xsl:value-of select="description"/></td>
                      </tr>
                  </xsl:for-each>
                </table>
          </body>
      </html>
    </xsl:template>
</xsl:stylesheet>