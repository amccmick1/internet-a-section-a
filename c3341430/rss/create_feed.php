<?php
// Get Database Credentials
require_once '../config/config.php';

// Include Database Class
require_once ('../includes/classes/MysqliDb.php');

// Creaate new Object of Database Class
$db = new MysqliDb ($dbc['hostname'], $dbc['username'], $dbc['password'], $dbc['database']);

// XML Parsing Function
function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

// Fetch Data
$tunes = $db->get ('tunes');

// Set XML Header
header("Content-Type: text/xml");

// Start XML file, echo parent node
$xml = '<?xml version="1.0" encoding="utf-8"?>';
// XSL File for use only with export_xml not in RSS
//$xml .= '<?xml-stylesheet type="text/xsl" href="style.xsl"?[REMOVEME]>';
$xml .= '<rss version="2.0">';
$xml .= '<channel>';
$xml .= '<title>RSS Feed</title>';
$xml .= '<link>http://54.68.21.95/AIDc3341430/rss/create_feed.php</link>';
$xml .= '<description>Top 10</description>';

// Iterate through the rows, printing XML nodes for each
if ($db->count > 0) {
    foreach ($tunes as $tune) {
          // ADD TO XML DOCUMENT NODE
          $xml .= '<item>';
          $xml .= "<title>".parseToXML($tune['song'])."</title>";
          $xml .= "<description>".parseToXML($tune['artist'])."</description>";
          $xml .= '</item>';
	}
}
// End XML file
$xml .= '</channel>';
$xml .= '</rss>';

echo $xml;
?>