<!-- Contact Form Submit -->
<script>
	$('#contact_form').submit(function() {
	
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
	
		$.ajax({
			url: formURL;
			type: "POST",
			data: postData,
			success: function(data) {
				//$("#success").html('<div class="12u"><style scoped> p { color: #339933; } </style><p>Message has been sent successfully.</p></div>');
			}
		});
		
		// Prevent Default Action of Form Submit
		e.PreventDefault();
	});
</script>

<section id="contact" class="main style3 secondary">
	<div class="content container">
		<header>
			<h2>Say Hello.</h2>
		</header>
		<div class="box container small">
		
			<!-- Contact Form Post Data Processing -->
			<?php require 'includes/functions/submit_contact_form.php'; ?>
			
			<!-- Contact Form -->
			<form id="contact_form" method="post">
				<div class="row half">
					
					<!-- Confirmation of Contact Form Submit -->
					<div id="success" class="row"></div>
					
					<!-- Contact Form -->
					<div class="6u"><input type="text" name="name" placeholder="Name" required /></div>
					<div class="6u"><input type="email" name="email" placeholder="Email" pattern="([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})" required /></div>
				</div>
				<div class="row half">
					<div class="12u"><textarea id="counttextarea" name="message" placeholder="Message" rows="6" required></textarea></div>
					<p><span id="countchars"></span> Characters Remaining</p>
				</div>
				<div class="row">
					<div class="row half">
						<div class="12u">
							<p>
								<?php
									require_once 'includes/classes/BasicMathSecurity.php';
									$math = new BasicMathSecurity( 'math' );
									echo $math->getField();
								?>
							</p>									
						</div>
					</div>
				</div>
				<div class="row">
					<div class="12u">
						<ul class="actions">
							<li><input type="submit" value="Send Message" /></li>
						</ul>
					</div>
				</div>
			</form>
				
		</div>
	</div>
</section>