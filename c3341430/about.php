<section id="one" class="main style2 right dark fullscreen">
	<div class="content box style2">
		<header>
			<h2>About</h2>
		</header>
		<p>This website has been created to showcase the web development skills I have acquired throughout my BSC(Hons) Computing course
		at Leeds Beckett University. Technologies used in this site include html5, mysqli, curl, xml, json, php5, css3 and jQuery.</p> 
	</div>
	<a href="#two" class="button style2 down anchored">Next</a>
</section>
